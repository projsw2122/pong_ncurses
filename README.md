# Pong

![Screenshot](/screenshots/pong.png)

A simple pong game created in C++ with ncurses.

## Adapted by

Original version: 
2016 Zachary Vincze https://github.com/zacharyvincze/Pong.git

FEUP's version: 
Armando Sousa (asousa@fe.up.pt) and Luís Moutinho for Software Design M.EEC course at FEUP


## Licence
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.



## Make and Makefile
The minimalistic makefile is especially well designed, take a look!

In the root directory use `make` or `make build` to compile the program.  This will create a `./pong` executable in the root directory of the project.

* To run the program, use either `make run` or `./pong` on the root directory of the project.
* To remove the program, use `make clean` or just `rm ./pong`.
* If you feel like doing a manual compile.  You can use `g++ src/main.cpp src/Player.cpp src/Ball.cpp -lncurses -o pong`.


Attention:
 * May need to change the INC of the makefile (adapt the path to your system!)

## Installation
You may need to install ncurses depending on what kind of system you're using.

* To install ncurses on a Debian based machine use `sudo apt-get install libncurses5-dev libncursesw5-dev`.
* To install ncurses on OS X use `brew install homebrew/dupes/ncurses`.  Of course, this will require [brew](http://brew.sh/ "Brew Homepage") to install.
* To install on windows, install MSYS2 and then `pacman -S mingw-w64-x86_64-ncurses` as per https://packages.msys2.org/package/mingw-w64-x86_64-ncurses

## Testing
Tested in windows VSCode 1.61, MSYS2 g++ 10.3.0
Should be portable...

## Compile & Execute

I had to adapt the `Makefile` so that VSCode would find the ncurses.h; adapt this file path if needed...

To copile, run `make` in the terminal

To execute in windows, run `.\pong.exe` 
To execute otherwise, run `.\pong`

## Controls
##### Player 1
* `W` - Move up
* `S` - Move down
* `A` - Move left
* `D` - Move right

##### Player 2
* `UP KEY` - Move up
* `DOWN KEY` - Move down
* `LEFT KEY` - Move left
* `RIGHT KEY` - Move right

To serve the ball, use the `SPACE` button.  It's the same for both players.

## Troubleshooting
I'm saving this section for later, trust me.  Something's gonna come up and I just want to be prepared.

If you have an issue with compiling or have found a bug in the game.  Take a look in the **Issues** section to see if the problem has been solved already.  If it's not, feel free to add the issue.  If it's big, it'll be posted in this troubleshooting section to help out other users.
