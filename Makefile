default: build

INC=-I"C:/msys64/mingw64/include/ncurses"

#INC=-Iinclude

build:
	@g++ src/main.cpp src/Player.cpp src/Ball.cpp -Iinclude -Wall $(INC) -lncurses -o pong || (echo "\n==> $(tput setaf 1)Make failed.  Do you have all the src files?\n$(tput sgr0)"; exit 1)
	@echo "\n==> $(tput setaf 2)Make successful.  Start the game using ./pong "

run:
	#./pong || true
	./pong

clean:
	rm pong
